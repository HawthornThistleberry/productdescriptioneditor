VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form frmDescriptionEditor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Product Description Editor v1.1"
   ClientHeight    =   5970
   ClientLeft      =   1050
   ClientTop       =   1455
   ClientWidth     =   7260
   Icon            =   "frmDescriptionEditor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5970
   ScaleWidth      =   7260
   Begin VB.CheckBox chkDisplayPOS 
      Caption         =   "POS"
      Enabled         =   0   'False
      Height          =   255
      Left            =   6240
      TabIndex        =   18
      Top             =   675
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CheckBox chkDisplayWeb 
      Caption         =   "Web"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5160
      TabIndex        =   17
      Top             =   675
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CommandButton cmdClearNew 
      Caption         =   "Clear New"
      Height          =   375
      HelpContextID   =   420
      Left            =   2520
      TabIndex        =   5
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CheckBox chkNewItem 
      Caption         =   "New Item"
      Enabled         =   0   'False
      Height          =   255
      HelpContextID   =   320
      Left            =   5160
      TabIndex        =   11
      Top             =   360
      Width           =   975
   End
   Begin VB.CommandButton cmdDel 
      Caption         =   "Delete Item"
      Enabled         =   0   'False
      Height          =   375
      HelpContextID   =   210
      Left            =   1920
      TabIndex        =   1
      Top             =   30
      Width           =   1695
   End
   Begin VB.CheckBox chkChanged 
      Caption         =   "Changed"
      Enabled         =   0   'False
      Height          =   255
      HelpContextID   =   330
      Left            =   6240
      TabIndex        =   12
      Top             =   360
      Width           =   1095
   End
   Begin VB.TextBox txtBrand 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   285
      HelpContextID   =   300
      Left            =   4275
      MaxLength       =   50
      TabIndex        =   9
      Top             =   0
      Width           =   2970
   End
   Begin VB.CommandButton cmdHTML 
      Caption         =   "Web"
      Enabled         =   0   'False
      Height          =   375
      HelpContextID   =   520
      Left            =   6360
      TabIndex        =   8
      Top             =   5280
      Width           =   885
   End
   Begin VB.CommandButton cmdAll 
      Caption         =   "All"
      Enabled         =   0   'False
      Height          =   375
      HelpContextID   =   510
      Left            =   5520
      TabIndex        =   7
      Top             =   5280
      Width           =   780
   End
   Begin VB.CommandButton cmdChanged 
      Caption         =   "Changed"
      Enabled         =   0   'False
      Height          =   375
      HelpContextID   =   500
      Left            =   4320
      TabIndex        =   6
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "Load"
      Height          =   375
      HelpContextID   =   400
      Left            =   0
      TabIndex        =   3
      Top             =   5280
      Width           =   975
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   375
      HelpContextID   =   410
      Left            =   1080
      TabIndex        =   4
      Top             =   5280
      Width           =   975
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add Item"
      Height          =   375
      HelpContextID   =   200
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   1695
   End
   Begin VB.ComboBox cmbSection 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   315
      HelpContextID   =   310
      ItemData        =   "frmDescriptionEditor.frx":0442
      Left            =   3675
      List            =   "frmDescriptionEditor.frx":046A
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   360
      Width           =   1380
   End
   Begin VB.TextBox txtDescription 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Letter Gothic"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4215
      HelpContextID   =   340
      Left            =   3675
      MultiLine       =   -1  'True
      TabIndex        =   13
      Top             =   960
      Width           =   3570
   End
   Begin VB.ListBox lisCodes 
      Height          =   4740
      HelpContextID   =   100
      Left            =   0
      TabIndex        =   2
      Top             =   480
      Width           =   3615
   End
   Begin MSComDlg.CommonDialog cdlGeneric 
      Left            =   4200
      Top             =   5040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ComctlLib.StatusBar sbrStatusBar 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   14
      Top             =   5670
      Width           =   7260
      _ExtentX        =   12806
      _ExtentY        =   529
      Style           =   1
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDisplayOn 
      Alignment       =   1  'Right Justify
      Caption         =   "Display on:"
      Height          =   255
      Left            =   3720
      TabIndex        =   19
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label lblBrand 
      Alignment       =   1  'Right Justify
      Caption         =   "Brand:"
      Height          =   255
      Left            =   3660
      TabIndex        =   16
      Top             =   45
      Width           =   495
   End
   Begin VB.Label lblWrite 
      Caption         =   "Write:"
      Height          =   255
      Left            =   3780
      TabIndex        =   15
      Top             =   5355
      Width           =   495
   End
End
Attribute VB_Name = "frmDescriptionEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Product Description Editor
' (c) 1997, 2000, Frank J. Perricone, Vermont Department of Liquor Control

' This program implements a simple (even simplistic) database of product descriptions for
' all items in our catalog.  Since only one such database is used, the data is stored in a
' simple delimited text file with a fixed filename.  Each item has a six-digit code (NABCA),
' a brand (the brand name; since several similar items may share a description, this
' brand name should not include things that differ, such as the size or proof), which
' section of the catalog it is in (whiskey, gin, etc.), and a text description.  The description
' is simply free-form, formatting-free text of up to 17 lines of 38 characters each (this
' limit is due to the screen size of the POS registers).  As a special case (though very
' common), an item's description may be another item's code number (all six digits must
' be present), which simply means it uses the same description as the listed code (in
' essence, the code number is a link).  It can also be blank.

' You can work as long as you like editing this database of codes and their descriptions,
' and the program keeps track of which have been changed, even over many sessions.
' When you're done, you can then press the Changed button to write out a file of all the
' changes, in a format that can be sent to the POS registers and used to update their
' database of product information.  You can also press the All button to create a similar
' file for all codes whether they've been changed or not.

' The format is one or more of the following:
'    nabca
'    [wrapped-description]
'    !!!

' In addition, the HTML button will create ten product information files, complete and
' ready to be posted, using header and footer files selected by the user and building the
' format of the intervening text automatically.  In this way one database can be used
' to create product information in both venues.

' Modifications:
'   11-29-2000  Added "Display On" checkboxes for items that shouldn't show on web or POS.


' configure VB4
Option Explicit
Option Base 0

' constants
Const conAppName = "Product Description Editor"
Const conVersion = 1.2
Const conFilename = "ProdDesc.txt"
Const conLstFilename = "ProdDesc.lst"
Const conHTMLHeader = "Header.html"
Const conHTMLFooter = "Footer.html"
Const conMaxItems = 2000
Const conMaxWidth = 38
Const conMaxHeight = 19
Const conDescriptionIndicator = " +"
Const conLinkIndicator = " �"
Const conNoDescIndicator = "   "
Const conChangedIndicator = "�"
Const conDeletedIndicator = " �"
Const conUnchangedIndicator = "   "
Const conNewItemIndicator = "!"
Const conOldItemIndicator = " "
Const conNoDescAvailableText = "<i>Descriptions are not available for all items yet.  Check back later for more.</i>"

' public variables
Public dirty As Boolean ' is there data in memory that needs saving to disk?
Public changes As Boolean ' are there any records which have been changed since last output of Changed records?
Public abortclose As Boolean ' should an error in LostFocus cancel a close?
Public LstFilename, HTMLheaderfile, HTMLfooterfile As String
Public edited_code As Integer ' code that the fields are using; we can't just use lisCodes.ListIndex
' because txtDescription_LostFocus happens after that's been changed by lisCodes_Click.

' database record type
Private Type typItem
  Code As String
  Brand As String
  Section As Integer ' 0-11, 0=whiskey, 11=variety
  NewItem As Boolean
  Changed As Boolean
  DisplayWeb As Boolean
  DisplayPOS As Boolean
  Description As String
End Type

' database in memory
Private aryCodeFile(conMaxItems) As typItem

' ***** FORM FUNCTIONS ***************************************************

' On startup, load registry settings, then load in the data file to start with
Private Sub Form_Load()
  ' position the form
  Left = GetSetting(conAppName, "Startup", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "Startup", "Top", (Screen.Height - Height) / 2)
  LstFilename = GetSetting(conAppName, "Files", "ListFile", conLstFilename)
  HTMLheaderfile = GetSetting(conAppName, "Files", "HTMLHeader", conHTMLHeader)
  HTMLfooterfile = GetSetting(conAppName, "Files", "HTMLFooter", conHTMLFooter)
  ' load in the data file and initialize the array
  dirty = False ' must be set first or cmdLoad_Click will ask if you really mean it
  Me.Show
  Me.Refresh
  edited_code = -1
  cmdLoad_Click
End Sub

' The only (normal) way out is to click the close box or the system menu.
Private Sub Form_Unload(Cancel As Integer)
  Dim i As Integer
  ' Make sure that there isn't unsaved data, and if there is, give 'em a chance to save it or cancel
  If dirty Then
      abortclose = False
      Call txtDescription_LostFocus
      If abortclose = True Then
          Cancel = True
          Exit Sub
        End If
      i = MsgBox("Save product descriptions first?", vbYesNoCancel + vbExclamation, conAppName)
      If i = vbCancel Then
          Cancel = 1
          Exit Sub
        End If
      If i = vbYes Then Call cmdSave_Click
    End If
  ' Save settings in the registry for next time
  Call SaveSetting(conAppName, "Startup", "Left", Left)
  Call SaveSetting(conAppName, "Startup", "Top", Top)
  Call SaveSetting(conAppName, "Files", "ListFile", LstFilename)
  Call SaveSetting(conAppName, "Files", "HTMLHeader", HTMLheaderfile)
  Call SaveSetting(conAppName, "Files", "HTMLFooter", HTMLfooterfile)
End Sub

' Update the state of all controls based on the current environment.  Called by anything
' that may change one of those states.  Microsoft, when is this going to be built into
' VB somehow directly?
Private Sub Form_EnableDisable()
  ' You can only save if there's something that needs saving.  Thus, the Save button is also
  ' an indicator of whether things need saving.
  If dirty Then cmdSave.Enabled = True Else cmdSave.Enabled = False
  ' You can only write out changes if something's been changed.
  If changes Then cmdChanged.Enabled = True Else cmdChanged.Enabled = False
  ' If there are no items, you can't change the current one, or write them all out, or write HTML
  If lisCodes.ListCount > 0 Then
      txtBrand.Enabled = True
      chkNewItem.Enabled = True
      chkDisplayWeb.Enabled = True
      chkDisplayPOS.Enabled = True
      cmbSection.Enabled = True
      txtBrand.BackColor = &H80000005
      cmbSection.BackColor = &H80000005
      txtDescription.BackColor = &H80000005
      cmdDel.Enabled = True
      If DescriptionIndicator(txtDescription) = conDeletedIndicator Then
          txtDescription.Enabled = False
          chkChanged.Enabled = False
          cmdDel.Caption = "Undelete Item"
        Else
          txtDescription.Enabled = True
          chkChanged.Enabled = True
          cmdDel.Caption = "Delete Item"
        End If
      cmdAll.Enabled = True
      cmdHTML.Enabled = True
    Else
      txtBrand.Enabled = False
      chkChanged.Enabled = False
      chkNewItem.Enabled = False
      chkDisplayWeb.Enabled = False
      chkDisplayPOS.Enabled = False
      cmbSection.Enabled = False
      txtDescription.Enabled = False
      txtBrand.BackColor = &HC0C0C0 ' gray background makes it clearer you can't edit it
      cmbSection.BackColor = &HC0C0C0
      txtDescription.BackColor = &HC0C0C0
      cmdDel.Enabled = False
      cmdAll.Enabled = False
      cmdHTML.Enabled = False
    End If
  ' if we have too many codes already, you can't make more
  If lisCodes.ListCount >= conMaxItems Then cmdAdd.Enabled = False Else cmdAdd.Enabled = True
End Sub

' ***** FIELD EDIT FUNCTIONS ************************************************

' When a code is selected, load its fields into the edit boxes and enable them
Private Sub lisCodes_Click()
  If lisCodes.ListCount <= 0 Then Exit Sub
  'Call txtDescription_LostFocus
  edited_code = lisCodes.ListIndex
  txtBrand = aryCodeFile(edited_code).Brand
  chkChanged = IIf(aryCodeFile(edited_code).Changed, 1, 0)
  chkNewItem = IIf(aryCodeFile(edited_code).NewItem, 1, 0)
  chkDisplayWeb = IIf(aryCodeFile(edited_code).DisplayWeb, 1, 0)
  chkDisplayPOS = IIf(aryCodeFile(edited_code).DisplayPOS, 1, 0)
  cmbSection.ListIndex = aryCodeFile(edited_code).Section
  txtDescription = aryCodeFile(edited_code).Description
  ' the title bar of the window shows what code you're working on
  Me.Caption = conAppName & " - " & aryCodeFile(edited_code).Code & " (" & _
                        aryCodeFile(edited_code).Brand & ")"
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array
Private Sub txtBrand_Change()
  If aryCodeFile(edited_code).Brand = txtBrand Then Exit Sub
  txtBrand = ConvertQuotes(txtBrand)
  aryCodeFile(edited_code).Brand = txtBrand
  dirty = True
  Call lisCodes_Update(edited_code)
  Me.Caption = conAppName & " - " & aryCodeFile(edited_code).Code & " (" & _
                        aryCodeFile(edited_code).Brand & ")"
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array and mark things as changed
Private Sub chkChanged_Click()
  Dim i As Integer
  If IIf(aryCodeFile(edited_code).Changed, 1, 0) = chkChanged Then Exit Sub
  aryCodeFile(edited_code).Changed = IIf(chkChanged = 1, True, False)
  Call lisCodes_Update(edited_code)
  changes = False
  For i = 0 To lisCodes.ListCount
    If aryCodeFile(i).Changed Then changes = True
    Next i
  dirty = True
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array
Private Sub chkNewItem_Click()
  If IIf(aryCodeFile(edited_code).NewItem, 1, 0) = chkNewItem Then Exit Sub
  aryCodeFile(edited_code).NewItem = IIf(chkNewItem = 1, True, False)
  Call lisCodes_Update(edited_code)
  dirty = True
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array
Private Sub chkDisplayWeb_Click()
  If IIf(aryCodeFile(edited_code).DisplayWeb, 1, 0) = chkDisplayWeb Then Exit Sub
  aryCodeFile(edited_code).DisplayWeb = IIf(chkDisplayWeb = 1, True, False)
  dirty = True
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array
Private Sub chkDisplayPOS_Click()
  If IIf(aryCodeFile(edited_code).DisplayPOS, 1, 0) = chkDisplayPOS Then Exit Sub
  aryCodeFile(edited_code).DisplayPOS = IIf(chkDisplayPOS = 1, True, False)
  dirty = True
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array and mark things as changed
Private Sub cmbSection_Click()
  If aryCodeFile(edited_code).Section = cmbSection.ListIndex Then Exit Sub
  aryCodeFile(edited_code).Section = cmbSection.ListIndex
  dirty = True
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array and mark things as changed
Private Sub txtDescription_Change()
  If aryCodeFile(edited_code).Description = txtDescription Then Exit Sub
  txtDescription = ConvertQuotes(txtDescription)
  aryCodeFile(edited_code).Description = txtDescription
  aryCodeFile(edited_code).Changed = True
  chkChanged.Value = 1
  Call lisCodes_Update(edited_code)
  changes = True
  dirty = True
  Call Form_EnableDisable
End Sub

' Save changes to the field into the array and mark things as changed
Private Sub txtDescription_LostFocus()
  Dim thisline, wordwrapped As String
  Dim lines, crlf As Integer
  If edited_code = -1 Then Exit Sub
  ' Strip off excess whitespace
  While Right$(txtDescription, 1) = Chr$(13) Or Right$(txtDescription, 1) = Chr$(10)
    txtDescription = Left$(txtDescription, Len(txtDescription) - 1)
    Wend
  ' Verify that, when wordwrapped, it's not more than conMaxHeight lines long
  wordwrapped = WordWrap(txtDescription, conMaxWidth)
  lines = 0
  Do While wordwrapped <> ""
    crlf = InStr(wordwrapped, Chr$(13) + Chr$(10))
    If crlf = 0 Then Exit Do
    thisline = Left(wordwrapped, crlf + 1)
    wordwrapped = Mid$(wordwrapped, crlf + 2)
    lines = lines + 1
    If thisline = "!!!" & Chr$(13) & Chr$(10) Then
        MsgBox "Warning: you cannot allow any line of the description to have the text '!!!'" & _
                     " alone on the line.  Please correct this.", vbOKOnly + vbExclamation, conAppName
        abortclose = True
        txtDescription.SetFocus
        lisCodes.ListIndex = edited_code
        Exit Sub
      End If
    Loop
  If lines > conMaxHeight Then
      MsgBox "Warning: description too long, it should fit in " & conMaxHeight & _
                   " lines (this is " & lines & ").  Please shorten it.", vbOKOnly + vbExclamation, conAppName
        abortclose = True
        txtDescription.SetFocus
        lisCodes.ListIndex = edited_code
      Exit Sub
    End If
  ' If it's a link, validate that it's a good one
  If txtDescription <> "" And Len(txtDescription) <= 6 And AllDigits(txtDescription) Then
      txtDescription = Right$("00000" & txtDescription, 6)
      ' validate it's a listed code, warn if not
      lines = aryCodeFile_BinarySearch(txtDescription)
      If DescriptionIndicator(aryCodeFile(lines).Description) = conLinkIndicator Then
          ' it's a link to a link; dereference the link to avoid this situation in the final files
          txtDescription = aryCodeFile(lines).Description
          lines = aryCodeFile_BinarySearch(txtDescription)
        End If
      If lines = -1 Then ' it's a link to a non-existent code
          MsgBox "Warning: " & txtDescription & " is not in the database.", _
                       vbOKOnly + vbInformation, conAppName
        ElseIf lines = edited_code Then ' it's a link to itself
          MsgBox "Error: you have linked this code's description to itself.", _
                       vbOKOnly + vbExclamation, conAppName
          abortclose = True
          txtDescription.SetFocus
          lisCodes.ListIndex = edited_code
        ElseIf aryCodeFile(lines).Section <> aryCodeFile(edited_code).Section Then
          ' it's a link to something in a different section
          MsgBox "Warning: " & txtDescription & " is in a different section of the price guide.", _
                       vbOKOnly + vbInformation, conAppName
        End If
      aryCodeFile(edited_code).Description = txtDescription
    End If
  Call Form_EnableDisable
End Sub

' ***** ADD AND DEL FUNCTIONS *********************************************

' The Add Code button is used to add an entry to the database.  (There is no way to delete
' one, because deleting the description still requires the row be kept long enough to write
' out the fact that it was deleted.  Once that is written, it will be deleted here.)
Private Sub cmdAdd_Click()
  Dim newcode As String
  Dim i As Integer
  ' get a code and left-pad it with zeroes
  newcode = InputBox("NABCA code of new item to add:", conAppName, "")
  If newcode = "" Then Exit Sub
  newcode = Right$("00000" & newcode, 6)
  ' insert it and then position the listbox on it and the cursor on the brand
  i = aryCodeFile_Insert(newcode, "", 0, True, True, True, True, "")
  dirty = True
  lisCodes.ListIndex = i
  txtBrand.SetFocus
End Sub

' This function deletes a code, but only if the description is empty *and* the changed flag is No.
' Otherwise, it just deletes the description.
Private Sub cmdDel_Click()
  If DescriptionIndicator(txtDescription) = conDeletedIndicator Then
      ' unmark the item deleted
      If MsgBox("Undelete item " & aryCodeFile(edited_code).Code & "?", _
                          vbYesNo + vbQuestion, conAppName) <> vbYes Then Exit Sub
      chkChanged.Value = 1
      txtDescription = ""
      txtDescription.SetFocus
    Else
      ' mark the item deleted
      If MsgBox("Delete item " & aryCodeFile(edited_code).Code & "?", _
                          vbYesNo + vbQuestion, conAppName) <> vbYes Then Exit Sub
      chkChanged.Value = 1
      txtDescription = "This item has been deleted. It will automatically disappear next time updates are sent out."
    End If
End Sub

' Clears the 'New Item' flag on all items
Private Sub cmdClearNew_Click()
  Dim i As Integer
  If MsgBox("Clear the 'New Item' flag for all items?", vbYesNo + vbQuestion, _
                  conAppName) <> vbYes Then Exit Sub
  sbrStatusBar.SimpleText = "Clearing 'New Item' flags..."
  sbrStatusBar.Refresh
  For i = 0 To lisCodes.ListCount
    aryCodeFile(i).NewItem = False
    Next i
  dirty = True
  sbrStatusBar.SimpleText = ""
  sbrStatusBar.Refresh
  Call lisCodes_Click
End Sub

' ***** ARRAY FUNCTIONS ***************************************************

' Whenever anyone adds a code, whether that's cmdLoad or cmdAddCode, this routine
' handles updating the array and the listbox
Private Function aryCodeFile_Insert(ByVal Code As String, ByVal Brand As String, _
                                                      ByVal Section As Integer, ByVal Changed As Boolean, _
                                                      ByVal NewItem As Boolean, ByVal DisplayWeb As Boolean, _
                                                      ByVal DisplayPOS As Boolean, ByVal Description As String) _
                                                      As Integer
  Dim i, j As Integer
  i = lisCodes.ListCount - 1
  ' find the place where it should be inserted, to keep 'em in order
  Do While i >= 0
    If aryCodeFile(i).Code = Code Then
        MsgBox "Warning: code " & Code & " already present.", vbOKOnly + vbExclamation, conAppName
        aryCodeFile_Insert = -1
        Exit Function
      End If
    If aryCodeFile(i).Code < Code Then Exit Do
    i = i - 1
    Loop
  i = i + 1
  ' i is now the location into which the item should be inserted
  aryCodeFile_Insert = i
  ' open up a space
  For j = lisCodes.ListCount To i Step -1
    aryCodeFile(j + 1).Code = aryCodeFile(j).Code
    aryCodeFile(j + 1).Brand = aryCodeFile(j).Brand
    aryCodeFile(j + 1).Section = aryCodeFile(j).Section
    aryCodeFile(j + 1).Changed = aryCodeFile(j).Changed
    aryCodeFile(j + 1).NewItem = aryCodeFile(j).NewItem
    aryCodeFile(j + 1).DisplayWeb = aryCodeFile(j).DisplayWeb
    aryCodeFile(j + 1).DisplayPOS = aryCodeFile(j).DisplayPOS
    aryCodeFile(j + 1).Description = aryCodeFile(j).Description
    Next j
  ' insert it into the space
  aryCodeFile(i).Code = Code
  aryCodeFile(i).Brand = Brand
  aryCodeFile(i).Section = Section
  aryCodeFile(i).Changed = Changed
  aryCodeFile(i).DisplayWeb = DisplayWeb
  aryCodeFile(i).DisplayPOS = DisplayPOS
  aryCodeFile(i).NewItem = NewItem
  aryCodeFile(i).Description = Description
  ' insert it into the listbox too
  lisCodes.AddItem Code, i
  Call lisCodes_Update(i)
'  & DescriptionIndicator(Description) & _
       IIf(Changed, conChangedIndicator, conUnchangedIndicator) & " " & Brand, i
End Function

' Deletes a code from the array and the list.
Private Sub aryCodeFile_Delete(ByVal position As Integer)
  Dim i As Integer
  For i = position To lisCodes.ListCount - 1
    aryCodeFile(i).Code = aryCodeFile(i + 1).Code
    aryCodeFile(i).Brand = aryCodeFile(i + 1).Brand
    aryCodeFile(i).Section = aryCodeFile(i + 1).Section
    aryCodeFile(i).Changed = aryCodeFile(i + 1).Changed
    aryCodeFile(i).NewItem = aryCodeFile(i + 1).NewItem
    aryCodeFile(i).DisplayWeb = aryCodeFile(i + 1).DisplayWeb
    aryCodeFile(i).DisplayPOS = aryCodeFile(i + 1).DisplayPOS
    aryCodeFile(i).Description = aryCodeFile(i + 1).Description
    Next i
  ' insert it into the listbox too
  lisCodes.RemoveItem position
  If position > lisCodes.ListCount - 1 Then position = lisCodes.ListCount - 1
  lisCodes.ListIndex = position
End Sub

' do a binary search for a code
Private Function aryCodeFile_BinarySearch(ByVal Code As String) As Integer
  Dim high, low, middle As Integer
  low = 0
  high = lisCodes.ListCount - 1
  While low <= high
    middle = Int((low + high) / 2)
    If aryCodeFile(middle).Code = Code Then
        aryCodeFile_BinarySearch = middle
        Exit Function
      ElseIf aryCodeFile(middle).Code < Code Then
        low = middle + 1
      Else
        high = middle - 1
      End If
    Wend
  aryCodeFile_BinarySearch = -1
End Function

' ***** LOAD AND SAVE FUNCTIONS *******************************************

' The user has clicked the Load button, or we're just starting up.  Load in the data.
Private Sub cmdLoad_Click()
  Dim ErrorText As String
  Dim inbuf As typItem
  ' if the current file is dirty, confirm reverting to saved data
  If dirty Then
      If MsgBox("Data has not been saved; really revert to what's on disk?", vbYesNo + vbQuestion, conAppName) <> vbYes Then Exit Sub
    End If
  lisCodes.Clear
  ' set up to load the file
  sbrStatusBar.SimpleText = "Loading product descriptions..."
  sbrStatusBar.Refresh
  changes = False
  ' open the file, creating it if necessary
  On Error GoTo createFile
  Open conFilename For Input As #1
  ' read the records in
  On Error GoTo fileerror
  Do While Not EOF(1)
    ' if we have too many records, abandon ship
    If lisCodes.ListCount >= conMaxItems Then
        MsgBox "Error: too many codes, I can only handle" & conMaxItems & _
                     ".  Discarding the rest.", vbOKOnly + vbExclamation, conAppName
        Exit Do
      End If
    ' read a record
    ErrorText = "reading product description database"
    Input #1, inbuf.Code, inbuf.Brand, inbuf.Section, inbuf.Changed, inbuf.NewItem, inbuf.DisplayWeb, inbuf.DisplayPOS, inbuf.Description
    If inbuf.Code = "X" Then Exit Do
    sbrStatusBar.SimpleText = "Loading product descriptions... " & inbuf.Code
    sbrStatusBar.Refresh
    ' if this has been changed, we know something has, so flag that
    If inbuf.Changed Then changes = True
    ' insert this item into our array
    ErrorText = "inserting a record"
    If aryCodeFile_Insert(inbuf.Code, inbuf.Brand, inbuf.Section, inbuf.Changed, inbuf.NewItem, inbuf.DisplayWeb, inbuf.DisplayPOS, inbuf.Description) = -1 Then
        If MsgBox("Error loading " & inbuf.Code & " (" & inbuf.Brand & "," & inbuf.Section & ").  Abort load?", vbYesNo + vbQuestion, conAppName) = vbYes Then Exit Sub
      End If
    Loop
  ErrorText = "closing file"
  Close #1
  GoTo done
createFile: ' make sure this isn't indicative of some problem
  If MsgBox("Data file not found; start with a blank one?", vbYesNo + vbQuestion, conAppName) <> vbYes Then End
done:
  If lisCodes.ListCount > 0 Then lisCodes.ListIndex = 0
  dirty = False
  Call Form_EnableDisable
  sbrStatusBar.SimpleText = ""
  Exit Sub
fileerror:
  If Err = 62 Then
      inbuf.Code = "X"
      Resume Next
    End If
  MsgBox "Load error " & ErrorText & ": " & Error(Err) & " (" & Err & "), aborting!", vbOKOnly + vbExclamation, conAppName
  Close #1
  dirty = False ' prevent any "Are you sure?" boxes from coming up
  End
End Sub

' The user has clicked on Save, or on Yes in a dirty program-close confirmation box.
Private Sub cmdSave_Click()
  Dim i As Integer
  Dim ErrorText As String
  sbrStatusBar.SimpleText = "Saving product descriptions..."
  sbrStatusBar.Refresh
  ' open the file, creating it if necessary
  ErrorText = "opening product file"
  On Error GoTo fileerror
  Open conFilename For Output As #1
  For i = 0 To lisCodes.ListCount - 1
    ErrorText = "writing records"
    Write #1, aryCodeFile(i).Code, aryCodeFile(i).Brand, aryCodeFile(i).Section, _
                   aryCodeFile(i).Changed, aryCodeFile(i).NewItem, aryCodeFile(i).DisplayWeb, _
                   aryCodeFile(i).DisplayPOS, aryCodeFile(i).Description
    sbrStatusBar.SimpleText = "Saving product descriptions... " & aryCodeFile(i).Code
    sbrStatusBar.Refresh
    Next i
  Close #1
  dirty = False
  Call Form_EnableDisable
  sbrStatusBar.SimpleText = ""
  Exit Sub
fileerror:
  MsgBox "Save error " & ErrorText & ": " & Error(Err) & " (" & Err & "), aborting!", vbOKOnly + vbExclamation, conAppName
  Close #1
End Sub

' The Changed button outputs a PRODDESC.LST file with only changed records,
' clearing the changed flags as it does.
Private Sub cmdChanged_Click()
  Dim i As Integer
  ' set up the common dialog for a save
  cdlGeneric.DialogTitle = "Select POS output file for changed codes"
  cdlGeneric.FileName = LstFilename
  cdlGeneric.Filter = "List Files (*.lst)|*.lst|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNNoChangeDir + cdlOFNOverwritePrompt + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  ' launch the dialog
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.FileName = "" Then Exit Sub
  ' open the file, saving the filename for later re-use
  sbrStatusBar.SimpleText = "Opening file..."
  sbrStatusBar.Refresh
  LstFilename = cdlGeneric.FileName
  Open LstFilename For Output As #1
  sbrStatusBar.SimpleText = "Writing changed records..."
  sbrStatusBar.Refresh
  For i = 0 To lisCodes.ListCount - 1
    If aryCodeFile(i).DisplayPOS And aryCodeFile(i).Changed Then
        sbrStatusBar.SimpleText = "Writing changed records... " & aryCodeFile(i).Code
        sbrStatusBar.Refresh
        ' write out this record; if it's a deleted item, write it blank, not the description
            Print #1, aryCodeFile(i).Code
        If DescriptionIndicator(aryCodeFile(i).Description) = conDeletedIndicator Then
            Print #1, "!!!"
            Call aryCodeFile_Delete(i)
            i = i - 1
          Else
            Print #1, WordWrap(aryCodeFile(i).Description, conMaxWidth) & "!!!"
          End If
        ' if we need to reset a changed flag, do so
        aryCodeFile(i).Changed = False
        ' remove the * in the listbox
        Call lisCodes_Update(i)
        'lisCodes.List(i) = aryCodeFile(i).Code & DescriptionIndicator(aryCodeFile(i).Description) & _
        '    " " & aryCodeFile(i).Brand
      End If
    Next i
  ' close up the file
  dirty = True
  changes = False
  sbrStatusBar.SimpleText = "Closing file..."
  sbrStatusBar.Refresh
  Close #1
  sbrStatusBar.SimpleText = ""
  Call lisCodes_Click
cancelled:
  Exit Sub
End Sub

' The All button outputs a PRODDESC.LST file with all records, whether or not they've changed,
' and optionally clears the changed flags as well.
Private Sub cmdAll_Click()
  Dim resetflags, i As Integer
  ' Should we clear the changed flag as we go?
  resetflags = MsgBox("Reset all 'changed' flags while writing?", vbYesNoCancel + vbQuestion, conAppName)
  If resetflags = vbCancel Then Exit Sub
  ' set up the common dialog for a save
  cdlGeneric.DialogTitle = "Select POS output file for all codes"
  cdlGeneric.FileName = LstFilename
  cdlGeneric.Filter = "List Files (*.lst)|*.lst|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNNoChangeDir + cdlOFNOverwritePrompt + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  ' launch the dialog
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.FileName = "" Then Exit Sub
  ' open the file, saving the filename for later re-use
  sbrStatusBar.SimpleText = "Opening file..."
  sbrStatusBar.Refresh
  LstFilename = cdlGeneric.FileName
  Open LstFilename For Output As #1
  sbrStatusBar.SimpleText = "Writing all records..."
  sbrStatusBar.Refresh
  For i = 0 To lisCodes.ListCount - 1
    sbrStatusBar.SimpleText = "Writing all records... " & aryCodeFile(i).Code
    sbrStatusBar.Refresh
    ' write out this record
    If aryCodeFile(i).DisplayPOS And DescriptionIndicator(aryCodeFile(i).Description) <> conDeletedIndicator Then
        Print #1, aryCodeFile(i).Code
        Print #1, WordWrap(aryCodeFile(i).Description, conMaxWidth) & "!!!"
      End If
    ' if we need to reset a changed flag, do so
    If resetflags = vbYes And aryCodeFile(i).Changed = True Then
        aryCodeFile(i).Changed = False
        Call lisCodes_Update(i)
        dirty = True
      End If
    Next i
  ' close up the file
  If resetflags = vbYes Then changes = False
  sbrStatusBar.SimpleText = "Closing file..."
  sbrStatusBar.Refresh
  Close #1
  sbrStatusBar.SimpleText = ""
  Call lisCodes_Click
cancelled:
  Exit Sub
End Sub

' Writes HTML files for all twelve categories.
Private Sub cmdHTML_Click()
  Dim Section, item, linkitem, nas As Integer
  Dim s, notavailable, ErrorText As String
  ' ask for locations of header and footer files
  cdlGeneric.DialogTitle = "Locate HTML Header file"
  cdlGeneric.FileName = HTMLheaderfile
  cdlGeneric.Filter = "HTML Files (*.htm,*.html)|*.htm*|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNNoChangeDir + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.FileName = "" Then Exit Sub
  HTMLheaderfile = cdlGeneric.FileName
  cdlGeneric.DialogTitle = "Locate HTML Footer file"
  cdlGeneric.FileName = HTMLfooterfile
  cdlGeneric.Filter = "HTML Files (*.htm,*.html)|*.htm*|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.FileName = "" Then Exit Sub
  HTMLfooterfile = cdlGeneric.FileName
  On Error GoTo fileerror
  ' for each category,
  For Section = 0 To 11
    ' Copy the header HTML file into the output, converting %codes as needed
    sbrStatusBar.SimpleText = "Generating HTML for " & SectionName(Section) & " section..."
    sbrStatusBar.Refresh
    ErrorText = "opening output file " & SectionName(Section) & ".html"
    Open LCase$(SectionName(Section)) & ".html" For Output As #1 '--!! file name may change
    ErrorText = "opening header file" '--!! will have to change header and footer files but not this code
    Open HTMLheaderfile For Input As #2
    Do While Not EOF(2) ' read lines from the header file one at a time
      ErrorText = "reading input from header file"
      Line Input #2, s
      If s = "X" Then Exit Do
      ' convert percent codes
      s = ConvertSubstrings(s, "%lsection", LCase(SectionName(Section)))
      s = ConvertSubstrings(s, "%section", SectionName(Section))
      s = ConvertSubstrings(s, "%secnum", Section)
      ErrorText = "writing header to section file" ' write them out
      Print #1, s
      Loop
    ErrorText = "closing header file"
    Close #2
    ' Now loop through all the codes and pick out those that are in this section
    notavailable = ""
    For item = 0 To lisCodes.ListCount - 1
      If aryCodeFile(item).DisplayWeb And aryCodeFile(item).Section = Section Then
          ' if it's deleted, don't write it
          If DescriptionIndicator(aryCodeFile(item).Description) = conDescriptionIndicator Then
              ' find all other codes that link to it and build the <a name> tags
              s = "<a name=" & Chr$(34) & "n" & aryCodeFile(item).Code & Chr$(34) & "></a>"
              For linkitem = 0 To lisCodes.ListCount - 1
                If linkitem <> item And aryCodeFile(linkitem).DisplayWeb And aryCodeFile(linkitem).Description = aryCodeFile(item).Code Then
                    s = s & "<a name=" & Chr$(34) & "n" & aryCodeFile(linkitem).Code & Chr$(34) & "></a>"
                  End If
                Next linkitem
              ' write out the record
              ' Print #1, "<h4>" & s & "<a href=" & Chr$(34) & "../priceguide/" & _
                            LCase$(SectionName(Section)) & ".html#n" & aryCodeFile(item).Code & Chr$(34) & ">" & _
                            aryCodeFile(item).Brand & "</a>" & IIf(aryCodeFile(item).NewItem, _
                            " <a href=" & Chr$(34) & "../priceguide/newitems.html" & Chr$(34) & _
                            "><img src=" & Chr$(34) & "../../images/new.png" & Chr$(34) & _
                            " width=23 height=9 align=absmiddle border=0 alt=" & Chr$(34) & "(new!)" & _
                            Chr$(34) & "></a>", "") & "</h4>"
              ' This is the code for the Drupal site:
              Print #1, "<h4>" & s & "<a href=" & Chr$(34) & "/price_guide/" & _
                            LCase$(SectionName(Section)) & "#n" & aryCodeFile(item).Code & Chr$(34) & ">" & _
                            aryCodeFile(item).Brand & "</a>" & IIf(aryCodeFile(item).NewItem, _
                            " <img src=" & Chr$(34) & "/sites/liquorretail/files/images/icons/new-badge.png" & Chr$(34) & _
                            " width=37 height=17 align=absmiddle border=0 alt=" & Chr$(34) & "(new!)" & _
                            Chr$(34) & "></a>", "") & "</h4>"
              Print #1, "<p>" & ConvertCRLFs(aryCodeFile(item).Description) & "</p>"
              Print #1, "<hr>"
            ElseIf DescriptionIndicator(aryCodeFile(item).Description) <> conDeletedIndicator Then ' this code has no description, add it to that list
              notavailable = notavailable & "<a name=" & Chr$(34) & "n" & aryCodeFile(item).Code & _
                            Chr$(34) & ">&nbsp;</a>"
            End If
        End If
      Next item
    If notavailable <> "" Then ' Add a not-available section
        Print #1, notavailable & "<p>" & conNoDescAvailableText & "</p>"
        Print #1, "<hr>"
      End If
    ErrorText = "opening footer file"
    Open HTMLfooterfile For Input As #2
    Do While Not EOF(2) ' read lines from the footer file one at a time
      ErrorText = "reading input from header file"
      Line Input #2, s
      If s = "X" Then Exit Do
      ' convert percent codes
      s = ConvertSubstrings(s, "%lsection", LCase(SectionName(Section)))
      s = ConvertSubstrings(s, "%section", SectionName(Section))
      s = ConvertSubstrings(s, "%secnum", Section)
      ErrorText = "writing footer to section file" ' write them out
      Print #1, s
      Loop
    ErrorText = "closing header file"
    Close #2
    ErrorText = "closing section file"
    Close #1
    Next Section
  sbrStatusBar.SimpleText = ""
  sbrStatusBar.Refresh
  Exit Sub
fileerror:
  If Err = 62 Then
      s = "X"
      Resume Next
    End If
  MsgBox "HTML generation error " & ErrorText & ": " & Error(Err) & " (" & Err & "), aborting!", vbOKOnly + vbExclamation, conAppName
  Close #1
cancelled:
  Exit Sub
End Sub

' ***** UTILITY FUNCTIONS **************************************************

' Update the lisCodes display
Private Sub lisCodes_Update(ByVal display_code As Integer)
  lisCodes.List(display_code) = aryCodeFile(display_code).Code & _
        DescriptionIndicator(aryCodeFile(display_code).Description) & _
        IIf(aryCodeFile(display_code).Changed, conChangedIndicator, conUnchangedIndicator) & _
        IIf(aryCodeFile(display_code).NewItem, conNewItemIndicator, conOldItemIndicator) & _
        " " & aryCodeFile(display_code).Brand
End Sub

' Word-wraps a string to a given length, inserting CR/LFs into the ends of lines (including the
' last one).  Exception: if the source is empty, so is the result.
Private Function WordWrap(ByVal source As String, ByVal length As Integer) As String
  Dim thisline, result As String
  Dim linebreak As Integer
  result = ""
  Do While source <> ""
    ' find the break for one line
    linebreak = InStr(source, Chr$(13) & Chr$(10))
    If Len(source) <= length Then
        thisline = source
        source = ""
      ElseIf linebreak <> 0 And linebreak <= length Then
        linebreak = InStr(source, Chr$(13) & Chr$(10))
        thisline = Left$(source, linebreak - 1)
        source = Mid$(source, linebreak + 2)
      Else
        linebreak = length + 1
        Do While Mid$(source, linebreak, 1) <> " "
          linebreak = linebreak - 1
          If linebreak = 0 Then
              linebreak = length
              Exit Do
            End If
          Loop
        ' linebreak now points at the last space to be included on the line.
        ' slice that line off into thisline
        thisline = Trim$(Left$(source, linebreak))
        source = Trim$(Mid$(source, linebreak + 1))
      End If
    ' attach it onto the result
    result = result & thisline & Chr$(13) & Chr$(10)
    Loop
  WordWrap = result
End Function

' Tells if a string is all digits, useful to tell if it's supposed to be a link
Private Function AllDigits(ByVal s As String) As Boolean
  Dim i As Integer
  Dim c As String
  AllDigits = False
  For i = 1 To Len(s)
    c = Mid$(s, i, 1)
    If c < "0" Or c > "9" Then Exit Function
    Next i
  AllDigits = True
End Function

' Converts all double-quotes to single-quotes.
Private Function ConvertQuotes(ByVal s As String) As String
  Dim i As Integer
  Dim c, result As String
  result = ""
  For i = 1 To Len(s)
    c = Mid$(s, i, 1)
    If c = Chr$(34) Then c = "'"
    result = result & c
    Next i
  ConvertQuotes = result
End Function

' Figures out the description indicator to show in lisCodes for the description.  Possible
' values are nothing, + for a text description, � for deleted, and > for a link.
Private Function DescriptionIndicator(ByVal Description As String) As String
  Description = Trim$(Description)
  While Right$(Description, 1) = Chr$(13) Or Right$(Description, 1) = Chr$(10)
    Description = Left$(Description, Len(Description) - 1)
    Wend
  If Left(Description, 27) = "This item has been deleted." Then
      DescriptionIndicator = conDeletedIndicator
    ElseIf Description = "" Then
      DescriptionIndicator = conNoDescIndicator
    ElseIf Len(Description) <= 6 And AllDigits(Description) Then
      DescriptionIndicator = conLinkIndicator
    Else
      DescriptionIndicator = conDescriptionIndicator
    End If
End Function

Private Function SectionName(ByVal Section As Integer) As String
  Select Case Section
    Case 0
      SectionName = "Whiskey"
    Case 1
      SectionName = "Gin"
    Case 2
      SectionName = "Vodka"
    Case 3
      SectionName = "Rum"
    Case 4
      SectionName = "Tequila"
    Case 5
      SectionName = "Brandy"
    Case 6
      SectionName = "Cordials"
    Case 7
      SectionName = "Cocktails"
    Case 8
      SectionName = "Vermouth"
    Case 9
      SectionName = "Wines"
    Case 10
      SectionName = "Beer"
    Case 11
      SectionName = "Variety"
    Case Else
      SectionName = "Unknown"
    End Select
End Function

' Converts one %code to its value in a string.
Private Function ConvertSubstrings(ByVal s As String, ByVal pcode As String, _
                                                      ByVal pvalue As String) As String
  Dim i As Integer
  i = InStr(s, pcode)
  While i <> 0
    s = Left$(s, i - 1) & pvalue & Mid$(s, i + Len(pcode))
    i = InStr(s, pcode)
    Wend
  ConvertSubstrings = s
End Function

' Converts CRLFs to </p>CRLF<p>.
Private Function ConvertCRLFs(ByVal s As String) As String
  Dim c, result As String
  Dim i As Integer
  For i = 1 To Len(s)
    c = Mid$(s, i, 1)
    If c = Chr$(13) Or c = Chr$(10) Then
        result = result & "</p>" & Chr$(13) & Chr$(10) & "<p>"
        i = i + 1 ' strip off all remaining CRs and LFs until the next something-else
        While i <= Len(s) And (Mid$(s, i, 1) = Chr$(13) Or Mid$(s, i, 1) = Chr$(10))
          i = i + 1
          Wend
        i = i - 1
      Else
        result = result & c
      End If
    Next i
  ConvertCRLFs = result
End Function
